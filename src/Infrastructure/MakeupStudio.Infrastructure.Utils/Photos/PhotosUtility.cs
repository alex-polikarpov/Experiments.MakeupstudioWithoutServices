﻿using System;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Photos;

namespace MakeupStudio.Infrastructure.Utils.Photos
{
    /// <summary>
    /// Утилита для работы с урлами
    /// </summary>
    internal class PhotosUtility : IPhotosUtility
    {
        #region Конструктор, поля, константы

        private readonly IFileSystemUtility _fileSystemUtility;
        private readonly string _baseUri;
        private const string _PHOTOS_DIRECTORY_NAME = "Photos";
        private readonly string _photosDirectory;
        private const string _NO_IMAGE_RELATIVE_PATH = @"\Content\Media\no_image.png";

        /// <summary>
        /// Создает утилиту для работы с фотографиями
        /// </summary>
        /// <param name="baseUri">Базовый урл сайта</param>
        public PhotosUtility(IFileSystemUtility fileSystemUtility, string baseUri)
        {
            _fileSystemUtility = fileSystemUtility;
            _baseUri = baseUri;
            var photosPath = _fileSystemUtility.CombinePath(_fileSystemUtility.FilesDirectory, _PHOTOS_DIRECTORY_NAME);
            _fileSystemUtility.CreateDirectory(photosPath);
            _photosDirectory = photosPath;
        }

        #endregion



        /// <summary>
        /// Генерирует случайное имя файла
        /// </summary>
        /// <param name="extension">Расширение файла вместе с точкой</param>
        public string GenerateFileName(string extension)
        {
            if (string.IsNullOrWhiteSpace(extension))
                return null;

            var fileName = Guid.NewGuid().ToString("N").ToLowerInvariant();
            return string.Format("{0}{1}", fileName, extension);
        }

        /// <summary>
        /// Перемещает файл в папку фотографий. Возвращает абсолютный путь к файлу (куда переместлии). 
        /// Имя файла может быть перегенерировано если файл с таким названием уже есть в папке фотографий
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу, который нужно переместить в папку фотографий</param>
        public string MoveToPhotosDirectory(string absolutePath)
        {
            var fileExtension = _fileSystemUtility.GetFileExtension(absolutePath);
            var fileName = _fileSystemUtility.GetFileName(absolutePath);
            var destPath = CreatePhotoPath(fileName);

            //Небольшая вероятность создания одинаковых гуидов есть, тем более за все время
            //жизни сайта фоток может быть загружено много
            while (_fileSystemUtility.FileIsExists(destPath))
            {
                fileName = GenerateFileName(fileExtension);
                destPath = CreatePhotoPath(fileName);
            }

            _fileSystemUtility.MoveFile(absolutePath, destPath);
            return destPath;
        }

        private string CreatePhotoPath(string fileName)
        {
            var firstDirName = fileName[0].ToString();
            var secondDirName = fileName[1].ToString();
            var firstDirPath = _fileSystemUtility.CombinePath(_photosDirectory, firstDirName);
            var secondDirPath = _fileSystemUtility.CombinePath(firstDirPath, secondDirName);
            _fileSystemUtility.CreateDirectory(secondDirPath);
            var destPath = _fileSystemUtility.CombinePath(secondDirPath, fileName);
            return destPath;
        }

        /// <summary>
        /// Возвращает абслютный урл к картинке исходя из относительного пути 
        /// </summary>
        /// <param name="relativePath">Относительный путь к картинке</param>
        public string GetFileAbsoluteUrl(string relativePath)
        {
            var builder = new UriBuilder(_baseUri);
            var relativeUrl = relativePath.Replace('\\', '/');
            builder.Path = relativeUrl;
            return builder.ToString();
        }

        /// <summary>
        /// Изменяет размер картинки и возвращает абсолютный урл на измененную картинку
        /// </summary>
        /// <param name="absoluteUrl">Абсолютный урл к картинке</param>
        /// <param name="width">ширина в пикселях</param>
        public string ResizeImage(string absoluteUrl, int width)
        {
            var builder = new UriBuilder(absoluteUrl);
            builder.Query = "width=" + width;
            return builder.ToString();
        }

        /// <summary>
        /// Изменяет размер картинки на указанный и возвращает абсолютный урл на новую картинку.
        /// Картинку не обрезает а сжимает при необходимости
        /// </summary>
        /// <param name="absoluteUrl">Абсолютный урл к картинке</param>
        /// <param name="width">ширина в пикселях</param>
        /// <param name="height">высота в пикселях</param>
        public string ResizeImage(string absoluteUrl, int width, int height)
        {
            var builder = new UriBuilder(absoluteUrl);
            builder.Query = string.Format("width={0}&height={1}&mode=carve", width, height);
            return builder.ToString();
        }

        public string GetNoImageRelativePath()
        {
            return _NO_IMAGE_RELATIVE_PATH;
        }
    }
}
