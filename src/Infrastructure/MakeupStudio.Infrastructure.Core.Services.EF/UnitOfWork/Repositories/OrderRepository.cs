﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable;
using MakeupStudio.Infrastructure.Core.Services.EF.UnitOfWork;
using MakeupStudio.Core;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    /// <summary>
    /// Сервис для работы с заказами
    /// </summary>
    internal class OrderRepository : EfRepository<Order>, IOrderRepository
    {
        public OrderRepository(DbContext context) : base(context)
        {
        }

        /// <summary>
        /// Возвращает новую заявку
        /// </summary>
        public async Task<Order> GetNotAcceptedOrder(Guid companyId, long orderId)
        {
            return await _dbContext.Set<Order>().AsNoTracking()
                .Where(x => x.Id == orderId)
                .FilterByCompanyId(companyId)
                .FilterByState(OrderState.NotAccepted)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает новые заявки
        /// </summary>
        /// <param name="companyId">Айди компании</param>
        public async Task<Order[]> GetNotAcceptedOrders(Guid companyId)
        {
            return await _dbContext.Set<Order>().AsNoTracking()
                .FilterByCompanyId(companyId)
                .FilterByState(OrderState.NotAccepted)
                .OrderBy(x=>x.CreatedDate)
                .ToArrayAsync();
        }

        /// <summary>
        /// Возвращает заказ
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public async Task<Order> GetOrder(Guid companyId, long orderId)
        {
            return await _dbContext.Set<Order>()
                .FilterByCompanyId(companyId)
                .Where(x => x.Id == orderId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Проверяет существует ли уже заявка от пользователя
        /// </summary>
        /// <param name="companyId">Id фирмы</param>
        /// <param name="clientPhone">Телефон клиента</param>
        public async Task<bool> BidIsExists(Guid companyId, string clientPhone)
        {
            //За какой период просматривать заявки от человека
            var period = DateTime.UtcNow.AddDays(-1);
            //Сколько заявок в день может человек отправлять
            var count = 3;

            var bidsCount = await _dbContext.Set<Order>().AsNoTracking()
                .FilterByCompanyId(companyId)
                .FilterByState(OrderState.NotAccepted)
                .Where(x => x.CreatedDate >= period)
                .CountAsync();

            return bidsCount >= count;
        }

        /// <summary>
        /// Можно ли изменить время записи заказа (нет ли уже назначенных заказов на это или близкое время)
        /// </summary>
        /// <param name="companyId">Айди фирмы</param>
        /// <param name="orderId">Айди заказа/заявки</param>
        /// <param name="newAppointedTime">Новое время записи</param>
        public async Task<bool> CanChangeAppointedTime(Guid companyId, long orderId, DateTime newAppointedTime)
        {
            var prevOrderTime = newAppointedTime.AddMinutes(-10);
            var afterOrderTime = newAppointedTime.AddMinutes(10);

            var hasAnotherOrders = await _dbContext.Set<Order>().AsNoTracking()
                .FilterByCompanyId(companyId)
                .FilterByState(OrderState.Accepted)
                .Where(x => x.Id != orderId)
                .Where(x => (x.AppointedTime >= prevOrderTime && x.AppointedTime <= newAppointedTime) ||
                            (x.AppointedTime >= newAppointedTime && x.AppointedTime <= afterOrderTime))
                .AnyAsync();

            return !hasAnotherOrders;
        }

        /// <summary>
        /// Возвращает предстоящие заказы 
        /// </summary>
        public async Task<Order[]> GetAcceptedOrders(Guid companyId)
        {
            return await _dbContext.Set<Order>().AsNoTracking()
                .FilterByCompanyId(companyId)
                .FilterByState(OrderState.Accepted)
                .OrderBy(x => x.AppointedTime)
                .ToArrayAsync();
        }
    }
}