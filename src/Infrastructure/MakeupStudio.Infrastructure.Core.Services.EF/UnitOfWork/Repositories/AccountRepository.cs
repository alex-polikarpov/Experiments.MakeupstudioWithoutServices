﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.UnitOfWork;
using MakeupStudio.Core;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    internal class AccountRepository : EfRepository<Account>, IAccountRepository
    {
        public AccountRepository(DbContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// Возвращает аккаунт по логину и паролю или null, если он не найден
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="passwordHash">хеш код пароля пользователя</param>
        public async Task<Account> GetAccountAsync(string login, string passwordHash)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(passwordHash))
                return null;

            var lowerLogin = login?.ToLower();

            return await _dbContext.Set<Account>()
                .Where(x => x.Email.ToLower() == lowerLogin && x.PasswordHash == passwordHash)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Проверяет есть ли пользователь с таким логином
        /// </summary>
        public async Task<bool> UserIsExistsAsync(string login)
        {
            login = login?.ToLower();
            return await _dbContext.Set<Account>()
                .Where(x => x.Email.ToLower() == login)
                .AnyAsync();
        }

        public async Task<bool> PhoneIsUsed(string phone)
        {            
            phone = phone?.ToLower();
            return await _dbContext.Set<Account>()
                .Where(x => x.Phone.ToLower() == phone)
                .AnyAsync();
        }

        public async Task<bool> UserAndCompanyIsActive(Guid companyId, int accountId)
        {
            return await _dbContext.Set<Account>().AsNoTracking()
                .Where(x => x.CompanyId == companyId && x.Id == accountId && x.AccountState == AccountState.Active)
                .Where(x => x.Company.CompanyState == CompanyState.Active)
                .AnyAsync();
        }
    }
}
