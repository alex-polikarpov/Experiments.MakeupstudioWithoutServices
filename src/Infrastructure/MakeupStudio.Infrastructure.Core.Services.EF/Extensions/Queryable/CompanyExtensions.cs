﻿using System.Linq;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable
{
    internal static class CompanyExtensions
    {
        /// <summary>
        /// Фильтрует фирмы по статусу
        /// </summary>
        public static IQueryable<Company> FilterByState(this IQueryable<Company> query, CompanyState state)
        {
            return query?.Where(x => x.CompanyState == state);
        }
    }
}