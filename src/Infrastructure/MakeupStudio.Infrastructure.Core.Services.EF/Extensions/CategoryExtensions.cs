﻿using MakeupStudio.Core.Entities;

namespace MakeupStudio.Infrastructure.Core.Services.EF.Extensions
{
    internal static class CategoryExtensions
    {
        public static Category SetName(this Category category, string name)
        {
            if (category != null)
                category.Name = name;

            return category;
        }

        public static Category SetCategoryType(this Category category, CategoryType type)
        {
            if (category != null)
                category.CategoryType = type;

            return category;
        }
    }
}
