﻿using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Данные для категории "Эпиляция"
    /// </summary>
    internal static class EpilationCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                Name = "Эпиляция",
                CategoryType = CategoryType.Works,
                ChildCategories = new Category[]
                {
                    new Category().SetName("Депиляция").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Шугаринг").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Лазерная эпиляция").SetCategoryType(CategoryType.Works),
                }
            });
        }
    }
}
