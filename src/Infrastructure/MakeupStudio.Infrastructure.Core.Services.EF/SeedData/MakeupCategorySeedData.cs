﻿using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Заполняет данные категории "Макияж"
    /// </summary>
    internal static class MakeupCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                Name = "Макияж",
                CategoryType = CategoryType.Works,
                ChildCategories = new Category[]
                {
                    new Category().SetName("Дневной макияж").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Свадебный макияж").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Вечерний макияж").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Фото-макияж (макияж для фотосессий)").SetCategoryType(CategoryType.Works)
                }
            });
        }
    }
}
