﻿using System.Collections.Generic;
using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Заполняет данные категории "Парикмахерские услуги"
    /// </summary>
    internal class HairdressingCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                CategoryType = CategoryType.Works,
                Name = "Парикмахерские услуги",
                ChildCategories = new Category[]
                {
                    CreateManCategory(),
                    CreateWomanCategory(),
                    CreateChildCategory(),
                    CreateColorationCategory(),
                    CreateHairStylesCategory(),
                    new Category().SetName("Химическая завивка").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Ламинирование").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Выпрямление").SetCategoryType(CategoryType.Works),
                }
            });
        }

        /// <summary>
        /// Создает категорию "Мужские стрижки"
        /// </summary>
        /// <returns></returns>
        private static Category CreateManCategory()
        {
            var category = new Category().SetName("Мужские стрижки").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Наголо"},
                new SystemWorkName() {Name = "Спортивная"},
                new SystemWorkName() {Name = "Модельная"},
                new SystemWorkName() {Name = "Креативная"},
                new SystemWorkName() {Name = "Окантовка"},
                new SystemWorkName() {Name = "Борода, усы"},
            };
            return category;
        }

        /// <summary>
        /// Создает категорию "Женские стрижки"
        /// </summary>
        private static Category CreateWomanCategory()
        {
            var category = new Category().SetName("Женские стрижки").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Короткие волосы"},
                new SystemWorkName() {Name = "Средние волосы"},
                new SystemWorkName() {Name = "Длинные волосы"},
                new SystemWorkName() {Name = "Креативная стрижка"},
                new SystemWorkName() {Name = "Стрижка горячими ножницами"},
                new SystemWorkName() {Name = "Коррекция длины"},
                new SystemWorkName() {Name = "Коррекция чёлки"},
            };
            return category;
        }

        /// <summary>
        /// Создает категорию "Детские стрижки"
        /// </summary>
        private static Category CreateChildCategory()
        {
            var category = new Category().SetName("Детские стрижки").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Мальчик"},
                new SystemWorkName() {Name = "Девочка"},
            };

            return category;
        }

        /// <summary>
        /// Создает категорию "Окрашивание"
        /// </summary>
        private static Category CreateColorationCategory()
        {
            var category = new Category().SetName("Окрашивание").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Мелирование"},
                new SystemWorkName() {Name = "Колорирование"}
            };
            return category;
        }

        /// <summary>
        /// Создает услугу "Прически"
        /// </summary>
        private static Category CreateHairStylesCategory()
        {
            var category = new Category().SetName("Прически").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Свадебная"},
                new SystemWorkName() {Name = "Вечерняя"},
                new SystemWorkName() {Name = "Косы (плетение)"}
            };
            return category;
        }


    }
}
