﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MakeupStudio.Common.Constants;
using Entities = MakeupStudio.Core.Entities;

namespace MakeupStudio.Models.Account
{
    /// <summary>
    /// Модель для регистрации в личном кабинете мастера
    /// </summary>
    public class RegistrationModel
    {
        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [MinLength(LengthConstants.EMAIL_MIN, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(Entities.Account.EMAIL_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [EmailAddress(ErrorMessage = ValidationErrors.EMAIL_INCORRECT_FORMAT)]
        [DisplayName("Email")]        
        public string Email { get; set; }

        /// <summary>
        /// Наименование мастерской
        /// </summary>
        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [MaxLength(Entities.Company.NAME_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [DisplayName("Имя")]        
        public string Name { get; set; }

        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [RegularExpression(RegexConstants.PHONE, ErrorMessage = ValidationErrors.PHONE_INCORRECT_FORMAT)]
        [DisplayName("Телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [MinLength(LengthConstants.Password.MIN, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(LengthConstants.Password.MAX, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [RegularExpression(RegexConstants.PASSWORD, ErrorMessage = ValidationErrors.PASSWORD_INCORRECT_FORMAT)]                
        [DisplayName("Пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Улица, где находится мастерская
        /// </summary>
        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [MinLength(LengthConstants.STREET_MIN, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(Entities.Company.STREET_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]        
        [DisplayName("Улица")]
        public string Street { get; set; }

        /// <summary>
        /// Номер дома
        /// </summary>
        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [MaxLength(Entities.Company.HOUSE_NUMBER_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]        
        [DisplayName("Дом")]
        public string House { get; set; }
    }
}