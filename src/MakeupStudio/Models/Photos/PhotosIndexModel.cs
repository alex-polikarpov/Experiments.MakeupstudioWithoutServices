﻿namespace MakeupStudio.Models.Photos
{
    /// <summary>
    /// Модель для отображения главной страницы фоток
    /// </summary>
    public class PhotosIndexModel
    {
        /// <summary>
        /// Урл на аватарку
        /// </summary>
        public string AvatarUrl { get; set; }

        /// <summary>
        /// Фотографии
        /// </summary>
        public PhotoModel[] Photos { get; set; }
    }
}