﻿using MakeupStudio.Core.Dto;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Models.Works
{
    /// <summary>
    /// Моделья для метода GetCategoriesAndSystemWorkNames 
    /// </summary>
    public class GetCategoriesAndSystemWorkNamesModel
    {
        public CategoryInfoDto[] Categories { get; set; }

        public SystemWorkName[] SystemWorkNames { get; set; }
    }
}