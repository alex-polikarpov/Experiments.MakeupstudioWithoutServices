﻿namespace MakeupStudio.Models.Works
{
    public class EditWorkModel
    {
        /// <summary>
        /// Название основной категории
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Название дочерней категории
        /// </summary>
        public string ChildCategoryName { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string WorkName { get; set; }

        /// <summary>
        /// Айди услуги
        /// </summary>
        public int WorkId { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// Есть ли системные названия услуг 
        /// </summary>
        public bool HasSystemWorkNames { get; set; }

        /// <summary>
        /// Массив системных названий услуг
        /// </summary>
        public string[] SystemWorkNames { get; set; }
    }
}