﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MakeupStudio.Attributes
{
    /// <summary>
    /// Валидирует размер файла
    /// </summary>
    public class ValidateFileSizeAttribute : ValidationAttribute
    {
        private readonly int _maxSize;

        /// <summary>
        /// Валидирует размер файла
        /// </summary>
        /// <param name="maxSize">Максимальный размер файла в килобайтах</param>
        public ValidateFileSizeAttribute(int maxSize)
        {
            _maxSize = maxSize;
        }

        public override bool IsValid(object value)
        {
            if (_maxSize <= 0)
                return true;

            var file = value as HttpPostedFileBase;
            if (file != null)
            {
                return file.ContentLength <= _maxSize*1024;
            }

            return true;
        }
    }
}