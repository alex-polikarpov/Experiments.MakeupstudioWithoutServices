﻿using System.Web.Mvc;

namespace MakeupStudio.Controllers
{
    /// <summary>
    /// Контроллер для работы с информацией для клиентов
    /// </summary>
    [Authorize]
    public class InformationForClientsController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
    }
}