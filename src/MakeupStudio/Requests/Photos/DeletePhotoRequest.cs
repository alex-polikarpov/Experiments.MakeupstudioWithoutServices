﻿namespace MakeupStudio.Requests
{
    /// <summary>
    /// Запрос на удаление фотографии
    /// </summary>
    public class DeletePhotoRequest
    {
        /// <summary>
        /// Id удаляемой фотографии
        /// </summary>
        public int PhotoId { get; set; }
    }
}