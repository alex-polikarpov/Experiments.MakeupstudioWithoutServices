﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace MakeupStudio.HtmlHelpers
{
    public static class ErrorsHtmlHelpers
    {
        /// <summary>
        /// Проверяет есть ли ошибки у поля
        /// </summary>
        public static bool ModelHasErrors<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            string modelName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));

            if (!htmlHelper.ViewData.ModelState.ContainsKey(modelName))
                return false;

            var modelErrors = htmlHelper.ViewData.ModelState[modelName]?.Errors;

            if (modelErrors == null || modelErrors.Count == 0)
                return false;

            var modelError = modelErrors.FirstOrDefault(m => !string.IsNullOrWhiteSpace(m.ErrorMessage));

            if (modelError == null)
                return false;

            return true;
        }

        /// <summary>
        /// Проверяет есть ли ошибки у поля и если есть возвращает имя класса has-errors иначе пустоту
        /// </summary>
        public static string GetHasErrorsClass<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            string modelName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));

            if (!htmlHelper.ViewData.ModelState.ContainsKey(modelName))
                return null;

            var modelErrors = htmlHelper.ViewData.ModelState[modelName]?.Errors;

            if (modelErrors == null || modelErrors.Count == 0)
                return null;

            var modelError = modelErrors.FirstOrDefault(m => !string.IsNullOrWhiteSpace(m.ErrorMessage));

            if (modelError == null)
                return null;

            return "has-error";
        }


        /// <summary>
        /// Возвращает ошибку валидации модели
        /// </summary>
        public static string GetModelError<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            string modelName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));

            if (!htmlHelper.ViewData.ModelState.ContainsKey(modelName))
                return string.Empty;

            var modelErrors = htmlHelper.ViewData.ModelState[modelName]?.Errors;

            if (modelErrors == null || modelErrors.Count == 0)
                return string.Empty;

            var modelError = modelErrors.FirstOrDefault(m => !string.IsNullOrWhiteSpace(m.ErrorMessage));

            if (modelError == null)
                return string.Empty;

            return modelError.ErrorMessage;
        }
    }
}