﻿using MakeupStudio.Responses;

namespace MakeupStudio.Extensions
{
    /// <summary>
    /// Расширения для респонсов
    /// </summary>
    public static class ResponseExtensions
    {
        /// <summary>
        /// Добавляет ошибку к респонсу
        /// </summary>
        public static T AddError<T>(this T response, string message, int errorCode) where T : BaseResponse
        {
            response.ErrorMessage = message;
            response.ErrorCode = errorCode;
            return response;
        }
    }
}