﻿$(document).ready(function() {
    
    //=============================================================
    //=============================================================
    // Константы, переменные
    //=============================================================
    //=============================================================
    var $errorContainer = $("#error-msg-container");
    var $errorText = $("#error-msg");
    var localDateSelector = ".localdate";
    var $appointedDate = $("#appointedTime");
    var $orderId = $("#orderId");
    var $masterComment = $("#masterComment");
    var $btnSave = $("#saveBtn");

    //=============================================================
    //=============================================================
    // методы для работы с контролами
    //=============================================================
    //=============================================================
    function saveBtn_OnClick() {
        _hideError();
        var appointedTime = $appointedDate.val();

        if (!appointedTime) {
            _showError("Выберите дату и время приема");
            return;
        }

        _save(appointedTime).done(function () {
            location.href = "/Orders/";
        });
    }


    //=============================================================
    //=============================================================
    // AJAX методы и вспомогательные методы
    //=============================================================
    //=============================================================

    function _save(appointedTime) {
        var date = _getDateFromString(appointedTime);
        var orderId = $orderId.val();
        var masterComment = $masterComment.val();

        var $deferred = $.Deferred();

        $.ajax({
            type: "POST",
            url: "/Orders/AppointMeeting",
            data : {
                OrderId: orderId,
                AppointedTime: date.toISOString(),
                MasterComment : masterComment
            },
            success: function (data) {
                if (data.ErrorCode && data.ErrorCode != 0) {                    
                    _showError(data.ErrorMessage);
                    $deferred.reject();
                } else {
                    $deferred.resolve();
                }                
            },
            error: function (data) {
                $deferred.reject();
                _showError("Внутренняя ошибка сервера, перезагрузите страницу");
            }
        });

        return $deferred.promise();
    }

    //Устанавливаем локальное время в полях
    //
    function _setLocalTime() {
        var $dates = $(localDateSelector);

        $.each($dates, function (index, elem) {
            var $element = $(elem);
            var utcDateTime = $element.data("date");

            if (utcDateTime) {
                var localDate = new Date(utcDateTime);
                $element.val(_convertToLocalTime(localDate));
            }            
        });
    }

    //Конвертирует дату в локальное время и формирует строку с датой
    //
    function _convertToLocalTime(localDate) {
        var date = new Date(localDate);
        var str = '';
        var val;

        str += (val = date.getDate()) < 10 ? "0" + val + "." : val + ".";
        str += (val = date.getMonth() + 1) < 10 ? "0" + val + "." : val + ".";
        str += date.getFullYear() + " ";

        str += (val = date.getHours()) < 10 ? "0" + val + ":" : val + ":";
        str += (val = date.getMinutes()) < 10 ? "0" + val : val;
        return str;
    }

    //Преобразует строковое представление даты в дату
    //
    function _getDateFromString(stringVal) {
        if (!stringVal)
            return null;

        var year, month, day;
        var hour = 0, minutes = 0;
        var split = stringVal.split(" ");

        if (!split || split.length == 0)
            return null;

        if (split.length > 0) {
            var dateSplit = split[0].split(".");

            if (!dateSplit || dateSplit.length != 3)
                return null;

            day = dateSplit[0];
            month = dateSplit[1];
            year = dateSplit[2];
        }

        if (split.length > 1) {
            var timeSplit = split[1].split(":");

            if (!timeSplit || timeSplit.length != 2)
                return null;

            hour = timeSplit[0];
            minutes = timeSplit[1];
        }

        return new Date(year, month, day, hour, minutes);
    }

    //Отображает ошибку
    //
    function _showError(errorMsg) {
        $errorText.text(errorMsg);
        $errorContainer.show();
    }

    //Скрывает ошибку
    //
    function _hideError() {
        $errorContainer.hide();
    }


    //=============================================================
    //=============================================================
    //INIT
    //=============================================================
    //=============================================================

    function initDatePicker() {
        var date = new Date();
       
        $appointedDate.datetimepicker({
            autoclose: true,
            todayBtn: true,
            startDate: date,
            maxView: 3,
            language: "ru",
            startView: 1,
            format: 'dd.mm.yyyy hh:ii'
        });
    }

    function init() {
        _setLocalTime();
        initDatePicker();
        $btnSave.click(saveBtn_OnClick);
    }

    init();
});