﻿$(document).ready(function () {

    //=============================================================
    //=============================================================
    // Константы
    //=============================================================
    //=============================================================
    var categoriesCache = undefined;
    var $categoryDdl = $("#category");
    var $childCategoryDdl = $("#childCategory");
    var $saveBtn = $("#saveBtn");
    var $workNameTxt = $("#work-name");
    var $workNamekDdl = $("#work-name-ddl");
    var $workNameBtn = $("#work-name-btn");
    var $workPrice = $("#work-price");
    var $errorContainer = $("#error-msg-container");
    var $errorText = $("#error-msg");
    


    //=============================================================
    //=============================================================
    //Методы для работы с контролами
    //=============================================================
    //=============================================================

    //Заполняет список главных категорий
    //
    function category_Fill() {

        _getCategories().done(function () {
            $.each(categoriesCache.Categories, function(index, category) {
                var option = $("<option>", {
                    value: category.Id,
                    text: category.Name
                });

                $categoryDdl.append(option);
            });

            $categoryDdl.change(category_OnChange);
            $categoryDdl.selectpicker('refresh');
        });
    }

    //Событие изменения главной категории
    //
    function category_OnChange() {
        var childCategories = _getChildCategories($(this).val());
        childCategory_Fill(childCategories);
        childCategory_SetDisabled(false);
        _setDisabled($saveBtn, true);
        _setDisabled($workNameTxt, true);
        _setDisabled($workNameBtn, true);
        workName_ResetText();
        workPrice_ResetText();
        _setDisabled($workPrice, true);
    }

    //Заполняет список дочерних категорий
    //
    function childCategory_Fill(childCategories) {

        $childCategoryDdl.empty();

        $.each(childCategories, function(index, childCategory) {
            var option = $("<option>", {
                value: childCategory.Id,
                text: childCategory.Name
            });

            $childCategoryDdl.append(option);
        });

        $childCategoryDdl.change(childCategory_OnChange);
        $childCategoryDdl.selectpicker('refresh');
    }

    //Управляет свойством disabled для контрола дочерней категории
    //
    function childCategory_SetDisabled(disabled) {
        _setDisabled($childCategoryDdl, disabled);
        $childCategoryDdl.selectpicker('refresh');
    }

    //Событие изменения дочерней категории
    //
    function childCategory_OnChange() {       
        _setDisabled($saveBtn, false);
        _setDisabled($workNameTxt, false);
        workName_FillDdl($(this).val());
        workName_ResetText();
        workPrice_ResetText();
        _setDisabled($workPrice, false);
    }

    //Заполняет выпадающий список с названиями услуг
    //
    function workName_FillDdl(childCategoryId) {
        var workNames = _getSystemWorkNames(childCategoryId);

        if (!workNames || workNames.length > 0) {
            _setDisabled($workNameBtn, false);
        };

        $workNamekDdl.empty();

        $.each(workNames, function(index, item) {
            var $li = $('<li><a href="#">' + item + '</a></li>');
            $li.click(workName_OnClickSystemWorkName);
            $workNamekDdl.append($li);
        });
    }

    //При выборе системного имени
    //
    function workName_OnClickSystemWorkName() {
        $workNameTxt.val($(this).text());
    }

    //Сбрасывает текст в текстбоксе названия услуги
    //
    function workName_ResetText() {
        $workNameTxt.val("");
    }

    //Сбрасывает текст в текстбоксе цены
    //
    function workPrice_ResetText() {
        $workPrice.val("");
    }

    //Обработчик события нажатия кнопки сохранить
    //
    function btnSave_OnClick() {
        _hideError();
        _addWork();
    }

    //=============================================================
    //=============================================================
    // AJAX методы и вспомогательные методы
    //=============================================================
    //=============================================================

    //Получает список категорий, подкатегорий и системных названий услуг
    //
    function _getCategories() {

        var $deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: "/Works/GetCategoriesAndSystemWorkNames",
            success: function (data) {
                categoriesCache = data;
                $deferred.resolve();
            },
            error: function (data) {
                $deferred.reject();
                _showError("Внутренняя ошибка сервера, перезагрузите страницу");
            }
        });

        return $deferred.promise();
    }

    //ajax метод для добавления услуги
    //
    function _addWork() {
        $.ajax({
            type: "POST",
            url: "/Works/CreateWork",
            data: {
                CategoryId: $childCategoryDdl.val(),
                WorkName: $workNameTxt.val(),
                Price: $workPrice.val()
            },
            success: function (data) {
                if (data.ErrorCode && data.ErrorCode != 0) {
                    _showError(data.ErrorMessage);
                } else {
                    location.href = "/Works";
                }
            },
            error: function (data) {
                _showError(data);
            }
        });
    }

    //Отображает ошибку
    //
    function _showError(errorMsg) {
        $errorText.text(errorMsg);
        $errorContainer.show();
    }

    //Скрывает ошибку
    //
    function _hideError() {
        $errorContainer.hide();
    }

    //Получает массив дочерних категорий по айди родительской категории
    //
    function _getChildCategories(categoryId) {

        var parentCategory = undefined;
        $.each(categoriesCache.Categories, function (index, category) {
            if (category.Id == categoryId) {                
                parentCategory = category;
            }
        });

        if (parentCategory)
            return parentCategory.ChildCategories;

        return [];
    }

    //Получает список системных названий услуг по айди дочерней категории
    //
    function _getSystemWorkNames(childCategoryId) {

        var systemWorks = [];
        $.each(categoriesCache.SystemWorkNames, function (index, workName) {
            if (workName.CategoryId == childCategoryId)
                systemWorks.push(workName.Name);
        });

        return systemWorks;
    }

    //Устанавливает свойство Disabled у контрола jquery
    //
    function _setDisabled($control, disabledValue) {
        var value = false;
        if (disabledValue == true)
            value = true;

        $control.attr("disabled", value);
    }


    //=============================================================
    //=============================================================
    //INIT
    //=============================================================
    //=============================================================
    function init() {
        category_Fill();
        childCategory_SetDisabled(true);
        _setDisabled($saveBtn, true);
        _setDisabled($workNameTxt, true);
        _setDisabled($workNameBtn, true);
        _setDisabled($workPrice, true);
        $saveBtn.click(btnSave_OnClick);
    }

    init();
});