﻿$(document).ready(function() {

    var $input = $("#avatar");

    $input.fileinput({
        showPreview: false,
        showRemove: false,
        showUpload: false,
        browseLabel : "Выбрать"
    });
})