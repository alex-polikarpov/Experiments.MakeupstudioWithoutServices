﻿using System.Net.Http;
using System.Web.Http;

namespace MakeupStudio.Api.Controllers
{
    /// <summary>
    /// Базовый контроллер апи
    /// </summary>
    public abstract class BaseApiController : ApiController
    {
        /// <summary>
        /// Резолвит сервис с помощью DI
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        public T Resolve<T>() where T:class
        {
            var diScope = Request.GetDependencyScope();
            return diScope.GetService(typeof (T)) as T;
        }
    }
}