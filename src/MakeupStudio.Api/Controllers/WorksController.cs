﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using MakeupStudio.Api.Responses.Works;
using MakeupStudio.Core;

namespace MakeupStudio.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы с услугами
    /// </summary>
    [RoutePrefix("Works")]   
    public sealed class WorksController : BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public WorksController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        [HttpGet, Route("Categories")]
        public async Task<JsonResult<CategoriesResponse>> GetCategories()
        {
            var response = new CategoriesResponse()
            {
                Categories = await _unitOfWork.WorkRepository.GetWorkCategoriesHierarchically()
            };
            return Json(response);
        }

        [HttpGet, Route("PriceLimits")]
        public async Task<JsonResult<PriceLimitsResponse>> PriceLimits()
        {            
            var limits = await _unitOfWork.WorkRepository.GetPriceLimits();
            var response = new PriceLimitsResponse()
            {
                MaxPrice = limits.MaxPrice,
                MinPrice = limits.MinPrice
            };
            return Json(response);
        }
    }
}