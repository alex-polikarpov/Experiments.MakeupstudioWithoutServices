﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using MakeupStudio.Api.Attributes;
using MakeupStudio.Api.Requests.Companies;
using MakeupStudio.Api.ResponseBuilders.Companies;
using MakeupStudio.Api.Responses.Companies;
using MakeupStudio.Common.Utils.Mapping;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core;

namespace MakeupStudio.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы с фирмами
    /// </summary>
    [RoutePrefix("Companies")]
    public sealed class CompaniesController : BaseApiController
    {
        private readonly IMapperUtility _mapperUtility;
        private readonly IPhotosUtility _photosUtility;
        private readonly IUnitOfWork _unitOfWork;

        public CompaniesController(IMapperUtility mapperUtility, IPhotosUtility photosUtility, IUnitOfWork unitOfWork)
        {
            _mapperUtility = mapperUtility;
            _photosUtility = photosUtility;
            _unitOfWork = unitOfWork;
        }


        /// <summary>
        /// Возвращает список активных компаний, для карты. 
        /// Возвращает только те компании, у которых есть услуги с установленной ценой
        /// </summary>
        [HttpGet]
        [Route("GetCompaniesForMap")]
        public async Task<JsonResult<GetCompaniesForMapResponse>> GetCompaniesForMap()
        {
            var builder = new GetCompaniesForMapResponseBuilder(_mapperUtility, _unitOfWork);
            var response = await builder.Build();           
            return Json(response);
        }

        /// <summary>
        /// Возвращает информацию по компаниям для превью в соответствии с фильтрами.
        /// </summary>
        [HttpGet]
        [ValidateRequest]
        [Route("GetCompaniesPreview")]
        public async Task<JsonResult<GetCompaniesPreviewResponse>> GetCompaniesPreview([FromUri]GetCompaniesPreviewRequest request)
        {
            var builder = new GetCompaniesPreviewResponseBuilder(_photosUtility, _unitOfWork);
            var response = await builder.Build(request);
            return Json(response);
        }

        [HttpGet]
        [Route("GetCompanyInfo")]
        public async Task<JsonResult<GetCompanyInfoResponse>> GetCompanyInfo([FromUri] Guid companyId)
        {
            var builder = new GetCompanyInfoResponseBuilder(_photosUtility, _unitOfWork);
            var response = await builder.Build(companyId);
            return Json(response);
        }
    }
}