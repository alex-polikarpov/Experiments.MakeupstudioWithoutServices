﻿using System;
using System.Configuration;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using MakeupStudio.Common.Constants;
using MakeupStudio.Common.Utils.Mapping;
using MakeupStudio.Infrastructure.Utils.DependencyResolution.Modules;
using MakeupStudio.Infrastructure.Utils.Mapping;
using Owin;

namespace MakeupStudio.Api
{
    public static class DependencyConfig
    {
        public static void Configure(IAppBuilder app)
        {            
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);

            builder.RegisterModule<ServicesModule>();
            var utilsModule = new UtilsModule(AppDomain.CurrentDomain.BaseDirectory,
                ConfigurationManager.AppSettings[KeysConstants.BASE_FILES_URI]);
            builder.RegisterModule(utilsModule);
            builder.RegisterModule<BusinessModule>();

            app.UseWebApi(config);

            //TODO СДЕЛАТЬ ПО НОРМАЛЬНОМУ
            var mapper = new AutoMapperUtilityImpl();
            mapper.Initialize(MappingConfig.Configure());
            builder.RegisterInstance(mapper).As<IMapperUtility>();
            //TODO

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}