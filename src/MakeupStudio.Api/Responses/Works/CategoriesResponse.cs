﻿using MakeupStudio.Core.Dto;

namespace MakeupStudio.Api.Responses.Works
{
    public class CategoriesResponse : BaseResponse
    {
        public CategoryInfoDto[] Categories { get; set; }
    }
}