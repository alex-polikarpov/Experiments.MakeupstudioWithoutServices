﻿using MakeupStudio.Api.Model;

namespace MakeupStudio.Api.Responses.Companies
{
    /// <summary>
    /// Ответ на запрос получения списка фирм для карты приложения
    /// </summary>
    public class GetCompaniesForMapResponse : BaseResponse
    {
        /// <summary>
        /// Фирмы
        /// </summary>
        public CompanyInfoModel[] Companies { get; set; }
    }
}