﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Api.Model;
using MakeupStudio.Api.Responses.Companies;
using MakeupStudio.Business.Errors;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Api.ResponseBuilders.Companies
{
    /// <summary>
    /// Билдер для респонса GetCompanyInfoResponse (получение информации для страницы мастера)
    /// </summary>
    public class GetCompanyInfoResponseBuilder
    {
        //Ширина фоток в пикселях
        private const int _PHOTOS_WIDTH = 210;
        private readonly IPhotosUtility _photosUtility;
        private readonly IUnitOfWork _unitOfWork;

        public GetCompanyInfoResponseBuilder(IPhotosUtility photosUtility, IUnitOfWork unitOfWork)
        {
            _photosUtility = photosUtility;
            _unitOfWork = unitOfWork;
        }



        public async Task<GetCompanyInfoResponse> Build(Guid companyId)
        {
            var result = new GetCompanyInfoResponse();

            //Информация о фирме
            //
            var companyInfo = await GetCompanyInfo(companyId);

            if (companyInfo == null)
                return CreateErrorResult(ErrorConstants.Companies.NOT_FOUND);

            result.CompanyInfo = companyInfo;

            //Информация об услугах и категориях
            //
            var works = await GetWorksInfo(companyId);

            if (works == null || works.Length < 1)
                return CreateErrorResult(ErrorConstants.Companies.NOT_FOUND);

            result.Categories = works;

            //Информация о фотках
            //          
            var photos = await _unitOfWork.PhotoRepository.GetCompanyPhotos(companyId);
            result.Avatar = GetAvatar(photos);
            result.Photos = GetPhotos(photos);
            return result;
        }

        private GetCompanyInfoResponse CreateErrorResult(string msg)
        {
            return new GetCompanyInfoResponse()
            {
                ErrorMessage = msg,
                ErrorCode = 100
            };
        }


        /// <summary>
        /// Формирует информацию о фирме
        /// </summary>
        private async Task<CompanyInfoModel> GetCompanyInfo(Guid companyId)
        {
            var companyInfo = await _unitOfWork.CompanyRepository.GetCompany(companyId);

            if (companyInfo == null)
                return null;

            return new CompanyInfoModel()
            {
                Name = companyInfo.Name,
                Id = companyInfo.Id,
                Street = companyInfo.Street,
                Longitude = companyInfo.Longitude,
                City = companyInfo.City,
                Description = companyInfo.Description,
                HouseNumber = companyInfo.HouseNumber,
                Latitude = companyInfo.Latitude
            };
        }


        private string GetAvatar(Photo[] photos)
        {
            var result = _photosUtility.GetFileAbsoluteUrl(_photosUtility.GetNoImageRelativePath());
                        
            var avatarPhoto = photos.FirstOrDefault(x => x.PhotoType == PhotoType.Avatar);
            if (avatarPhoto != null && !string.IsNullOrWhiteSpace(avatarPhoto.RelativePath))
            {
                result = _photosUtility.GetFileAbsoluteUrl(avatarPhoto.RelativePath);
            }

            return _photosUtility.ResizeImage(result, _PHOTOS_WIDTH);
        }

        private string[] GetPhotos(Photo[] photos)
        {
            var result = new List<string>();
            var albumPhotos = photos.Where(x => x.PhotoType == PhotoType.Album).ToArray();
            foreach (var photo in albumPhotos)
            {
                var photoUrl = _photosUtility.GetFileAbsoluteUrl(photo.RelativePath);
                result.Add(_photosUtility.ResizeImage(photoUrl, _PHOTOS_WIDTH));
            }

            return result.ToArray();
        }

        #region GetWorksInfo

        /// <summary>
        /// Формирует часть респонса с категориями и услугами
        /// </summary>
        private async Task<CategoryModel[]> GetWorksInfo(Guid companyId)
        {
            var works = await _unitOfWork.WorkRepository.GetCompanyWorks(companyId);

            if (works == null || !works.Any())
                return null;

            var workCategories = await _unitOfWork.WorkRepository.GetWorkCategories();
            return GetChildCategoriesWithWorks(null, workCategories, works);
        }


        /// <summary>
        /// Находит подкатегории, у которых есть услуги
        /// </summary>
        private CategoryModel[] GetChildCategoriesWithWorks(int? parentCategoryId, Category[] allCategories, Work[] works)
        {
            var result = new List<CategoryModel>();

            var categories = allCategories.Where(x => x.ParentCategoryId == parentCategoryId).ToArray();

            foreach (var category in categories)
            {
                var categoryWorks = GetCategoryWorks(category.Id, works);

                //Если у категории есть услуги, то подкатегории не ищем 
                if (categoryWorks != null && categoryWorks.Any())
                {
                    result.Add(new CategoryModel()
                    {
                        CategoryId = category.Id,
                        HasWorks = true,
                        Works = categoryWorks,
                        CategoryName = category.Name
                    });
                }
                else
                {
                    //Если услуг нет, пытаемся найти подкатегории, в которых есть услуги
                    var childCategories = GetChildCategoriesWithWorks(category.Id, allCategories, works);

                    if(childCategories == null || !childCategories.Any())
                        continue;

                    result.Add(new CategoryModel()
                    {
                        CategoryId = category.Id,
                        CategoryName = category.Name,
                        ChildCategories = childCategories,
                    });
                }
            }

            return result.ToArray();
        }

        /// <summary>
        /// Возвращает услуги категории
        /// </summary>
        private WorkModel[] GetCategoryWorks(int categoryId, Work[] works)
        {
            var categoryWorks = works.Where(x => x.CategoryId == categoryId).ToArray();
            if (!categoryWorks.Any())
                return null;

            return categoryWorks.Select(x => new WorkModel()
            {
                CategoryId = x.CategoryId,
                Name = x.Name,
                Id = x.Id,
                Price = x.Price
            }).ToArray();
        }

        #endregion
    }
}