﻿namespace MakeupStudio.Api.Model
{
    /// <summary>
    /// Модель для отображения услуги
    /// </summary>
    public class WorkModel
    {
        /// <summary>
        /// Айди услуги
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Айди категории
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Price { get; set; }
    }
}