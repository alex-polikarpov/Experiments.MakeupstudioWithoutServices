﻿namespace MakeupStudio.Common.Constants
{
    /// <summary>
    /// Кастомные клеймы для Asp.net Identity
    /// </summary>
    public static class CustomClaimsConstants
    {
        public const string COMPANY_ID = "CompanyId";

        public const string ACCOUNT_STATE = "AccountState";
    }
}