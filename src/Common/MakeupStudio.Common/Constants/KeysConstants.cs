﻿namespace MakeupStudio.Common.Constants
{
    /// <summary>
    /// Константы с ключами
    /// </summary>
    public static class KeysConstants
    {
        /// <summary>
        /// Ключ для базового урла где находятся фотки (только базовый урл)
        /// </summary>
        public const string BASE_FILES_URI = "BaseFilesUri";

        /// <summary>
        /// Константы для TempData
        /// </summary>
        public static class TempData
        {
            public const string ERROR_MESSAGE = "ErrorMsg";

            public const string SUCCESS_MESSAGE = "SuccessMsg";
        }
    }
}