﻿namespace MakeupStudio.Common.Constants
{
    //TODO перенести эти ошибки в Business.Errors и возможно надо Common.Errors назвать его 

    /// <summary>
    /// Константы с ошибками валидации
    /// </summary>
    public static class ValidationErrors
    {
        public const string REQUIRED = "Заполните поле";

        public const string EMAIL_INCORRECT_FORMAT = "Неверный формат электронной почты";

        public const string MIN_LENGTH = "Кол-во символов должно быть не меньше {1}";

        public const string MAX_LENGTH = "Кол-во символов должно быть не больше {1}";

        public const string PHONE_INCORRECT_FORMAT = "Неверный формат телефона";

        public const string PASSWORD_INCORRECT_FORMAT = "Пароль должен содержать цифры и буквы англ. алфавита";

        public const string CHOOSE_FILE = "Выберите файл";

        public const string INCORRECT_PHOTO_EXTENSION = "Допустимые форматы jpg, png, jpeg";

        public const string PHOTO_INCORRECT_SIZE = "Максимальный размер файла 2 мегабайта";

        public const string NAME_INCORRECT_FORMAT = "Имя должно состоять только из русских символов";

        public const string COMMENT_INCORRECT_FORMAT = "Недопустимые символы в комментарии. Ограничьтесь буквами, цифрами, пунктуацией";
    }
}