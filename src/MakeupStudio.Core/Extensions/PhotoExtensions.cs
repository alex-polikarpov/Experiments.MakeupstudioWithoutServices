﻿using System.Collections.Generic;
using System.Linq;

namespace MakeupStudio.Core.Entities.Extensions
{
    /// <summary>
    /// Методы расширения для фотографий
    /// </summary>
    public static class PhotoExtensions
    {
        /// <summary>
        /// фильтрует IEnumerable коллекцию фотографий по типу фотограции
        /// </summary>
        /// <param name="photos">IEnumerable коллекция фотографий</param>
        /// <param name="photoType">Тип фотографий, которые необходимо оставить</param>
        public static IEnumerable<Photo> FilterByPhotoType(this IEnumerable<Photo> photos, PhotoType photoType)
        {
            return photos?.Where(x => x.PhotoType == photoType);
        }
    }
}
