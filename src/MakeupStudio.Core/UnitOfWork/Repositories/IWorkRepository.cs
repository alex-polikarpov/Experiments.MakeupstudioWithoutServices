﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Core.Dto;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Core
{
    /// <summary>
    /// Репозиторий для работы с услугами
    /// </summary>
    public interface IWorkRepository : IRepository<Work>
    {
        /// <summary>
        /// Возвращает список категорий услуг(с подкатегориями, иерархично)
        /// </summary>
        Task<CategoryInfoDto[]> GetWorkCategoriesHierarchically();

        /// <summary>
        /// Возвращает список категорий услуг (не иерархично)
        /// </summary>
        Task<Category[]> GetWorkCategories();

        /// <summary>
        /// Ищет категорию по Id
        /// </summary>
        Task<Category> GetCategory(int categoryId);

        /// <summary>
        /// Возвращает информацию о минимально и максимально возможной цене на услугу
        /// </summary>
        Task<PriceLimitsDto> GetPriceLimits();

        /// <summary>
        /// Возвращает системные названия услуг
        /// </summary>
        Task<SystemWorkName[]> GetSystemWorkNames();

        /// <summary>
        /// Проверяет существует ли у фирмы услуга
        /// </summary>
        /// <param name="companyId">Id фирмы</param>
        /// <param name="categoryId">Id категории</param>
        /// <param name="workName">Название услуги</param>
        Task<bool> HasWorkName(Guid companyId, int categoryId, string workName);

        /// <summary>
        /// Возвращает услуги фирмы
        /// </summary>
        Task<WorkPreviewDto[]> GetWorkPreviews(Guid companyId);

        /// <summary>
        /// Предоставляет информацию по услуге
        /// </summary>
        Task<Work> GetWork(int workId, Guid companyId);

        /// <summary>
        /// Проверяет, можно ли изменить у услуги название
        /// </summary>
        /// <param name="workId">Id услуги, чтобы исключить из проверки ее саму</param>
        /// <param name="newName">новое название услуги</param>
        /// <param name="categoryId">айди категории, в которой находится изменяемая услуга</param>
        Task<bool> CanChangeWorkName(int workId, string newName, int categoryId);

        /// <summary>
        /// Возвращает информацию о минимальной цене на услуги для фирм
        /// </summary>
        /// <param name="companiesId">Айдишники фирм, для которых нужно предоставить информацию о мин. ценах</param>
        Task<CompanyMinPriceDto[]> GetCompaniesMinPrices(Guid[] companiesId);

        /// <summary>
        /// Возвращает информацию об услугах фирмы
        /// </summary>
        Task<Work[]> GetCompanyWorks(Guid companyId);
    }
}
