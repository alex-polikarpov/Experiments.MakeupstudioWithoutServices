﻿using MakeupStudio.Core.Entities;

namespace MakeupStudio.Core
{
    /// <summary>
    /// Репозиторий для работы с сущностями
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности, с которой работает репозиторий</typeparam>
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Добавляет новую сущность в коллекцию (без сохранения)
        /// </summary>
        TEntity Add(TEntity entity);

        /// <summary>
        /// Удаляет сущность из коллекции (без сохранения)
        /// </summary>
        void Remove(TEntity entity);

        /// <summary>
        /// Обновляет сущность в коллекции (без сохранения)
        /// </summary>
        TEntity Update(TEntity entity);
    }
}
