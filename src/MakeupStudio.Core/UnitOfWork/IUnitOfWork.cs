﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace MakeupStudio.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IAccountRepository AccountRepository { get; }

        ICompanyRepository CompanyRepository { get; }

        IOrderRepository OrderRepository { get; }

        IPhotoRepository PhotoRepository { get; }

        IWorkRepository WorkRepository { get; }

        /// <summary>
        /// Синхронно сохраняет изменения
        /// </summary>
        /// <exception cref="UnitOfWorkException">
        /// Возникает если не удалось по каким-либо причинам сохранить изменения
        /// </exception>
        void SaveChanges();


        /// <summary>
        /// Асинхронно сохраняет изменения
        /// </summary>
        /// <exception cref="UnitOfWorkException">
        /// Возникает если не удалось по каким-либо причинам сохранить изменения
        /// </exception>
        Task SaveChangesAsync();

        /// <summary>
        /// Открывает транзакцию
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Открывает транзакцию
        /// </summary>
        /// <param name="isolationLevel">Поведение транзакции</param>
        void BeginTransaction(IsolationLevel isolationLevel);

        /// <summary>
        /// Откатывает транзакцию
        /// </summary>
        void RollBackTransaction();

        /// <summary>
        /// Коммитит транзакцию
        /// </summary>
        void CommitTransaction();
    }
}
