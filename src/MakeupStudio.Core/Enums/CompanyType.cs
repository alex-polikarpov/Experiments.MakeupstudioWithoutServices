﻿namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Тип компании (частник или нет)
    /// </summary>
    public enum CompanyType
    {
        /// <summary>
        /// Физ лицо (частник)
        /// </summary>
        Individual = 0,

        /// <summary>
        /// Студия (где работает несколько человек)
        /// </summary>
        Studio = 1
    }
}
