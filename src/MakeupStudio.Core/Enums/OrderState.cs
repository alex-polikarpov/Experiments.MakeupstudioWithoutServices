﻿namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Состояние заказа
    /// </summary>
    public enum OrderState
    {
        /// <summary>
        /// Не принят (заявка пришла, но мастер ее еще не принял)
        /// </summary>
        NotAccepted = 0,

        /// <summary>
        /// Принят (мастер принял заявку)
        /// </summary>
        Accepted = 1,

        /// <summary>
        /// Выполнен
        /// </summary>
        Finished = 2
    }
}