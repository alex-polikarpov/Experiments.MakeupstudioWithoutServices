﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Фирма (частник или студия)
    /// </summary>
    [Table("Companies")]
    public class Company : IEntity<Guid>
    {
        public const int NAME_MAX_LENGTH = 150;
        public const int DESCRIPTION_MAX_LENGTH = 500;
        public const int CITY_MAX_LENGTH = 50;
        public const int STREET_MAX_LENGTH = 100;
        public const int HOUSE_NUMBER_MAX_LENGTH = 20;



        [Key]
        [Column("Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Название фирмы
        /// </summary>
        [Required]
        [MaxLength(NAME_MAX_LENGTH)]
        [Column("Name")]        
        public string Name { get; set; }

        /// <summary>
        /// Описание фирмы (это описание будут видеть пользователи)
        /// </summary>        
        [MaxLength(DESCRIPTION_MAX_LENGTH)]
        [Column("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Баланс фирмы (сколько у них денег на счету в нашей системе)
        /// </summary>
        [Column("Balance")]
        public decimal Balance { get; set; }

        /// <summary>
        /// Город, в котором находится фирма
        /// </summary>        
        [Required]
        [MaxLength(CITY_MAX_LENGTH)]
        [Column("City")]
        public string City { get; set; }

        /// <summary>
        /// Улица, на которой находится фирма
        /// </summary>
        [Required]
        [MaxLength(STREET_MAX_LENGTH)]
        [Column("Street")]
        public string Street { get; set; }

        /// <summary>
        /// Номер дома фирмы
        /// </summary>
        [Required]        
        [MaxLength(HOUSE_NUMBER_MAX_LENGTH)]
        [Column("HouseNumber")]
        public string HouseNumber { get; set; }

        /// <summary>
        /// Широта (местоположение фирмы)
        /// </summary>
        [Column("Latitude")]
        public double Latitude { get; set; }

        /// <summary>
        /// Долгота (местоположение фирмы)
        /// </summary>
        [Column("Longitude")]
        public double Longitude { get; set; }

        /// <summary>
        /// Состояние фирмы
        /// </summary>
        [Column("CompanyState")]
        public CompanyState CompanyState { get; set; }

        /// <summary>
        /// Тип фирмы (частник или студия)
        /// </summary>
        [Column("CompanyType")]
        public CompanyType CompanyType { get; set; }



        #region Навигационные свойства

        /// <summary>
        /// Навигационные свойства. Аккаунты фирмы
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Account> Accounts { get; set; }

        /// <summary>
        /// Навигационное свойство. Фотографии фирмы
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Photo> Photos { get; set; }

        /// <summary>
        /// Навигационное свойство. Услуги фирмы (которые фирма сама создала)
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Work> Works { get; set; }

        /// <summary>
        /// Навигационное свойство. Заказы фирмы
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Order> Orders { get; set; }

        #endregion
    }
}