﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Фотографии фирмы
    /// </summary>
    [Table("Photos")]
    public class Photo : IEntity<int>
    {
        public const int DESCRIPTION_MAX_LENGTH = 500;
        public const int PATH_MAX_LENGTH = 1000;



        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id фирмы, которой принадлежат фотки
        /// </summary>
        [ForeignKey(nameof(Company))]
        [Column("CompanyId")]
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Описание фотографии
        /// </summary>
        [MaxLength(DESCRIPTION_MAX_LENGTH)]
        [Column("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Относительный путь к файлу
        /// </summary>
        [Required]
        [MaxLength(PATH_MAX_LENGTH)]
        [Column("RelativePath")]
        public string RelativePath { get; set; }

        /// <summary>
        /// Тип фотографии
        /// </summary>
        [Column("PhotoType")]
        public PhotoType PhotoType { get; set; }



        #region Навигационные свойства

        /// <summary>
        /// Фирма, кому принадлежит эта фотка
        /// </summary>
        [JsonIgnore]
        public virtual Company Company { get; set; }

        #endregion
    }
}
