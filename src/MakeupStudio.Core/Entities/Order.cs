﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Заказ
    /// </summary>
    [Table("Orders")]
    public class Order : IEntity<long>
    {
        public const int CLIENT_PHONE_MAX_LENGTH = 20;
        public const int CLIENT_NAME_MAX_LENGTH = 100;
        public const int CLIENT_COMMENT_MAX_LENGTH = 200;
        public const int MASTER_COMMENT_MAX_LENGTH = 500;

        [Key]
        [Column("Id")]
        public long Id { get; set; }

        /// <summary>
        /// Id фирмы, которой принадлежит заказ
        /// </summary>
        [Column("CompanyId")]
        [ForeignKey(nameof(Company))]
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        [Required]
        [MaxLength(CLIENT_PHONE_MAX_LENGTH)]
        [Column("ClientPhone")]
        public string ClientPhone { get; set; }

        /// <summary>
        /// Имя клиента
        /// </summary>
        [Required]
        [MaxLength(CLIENT_NAME_MAX_LENGTH)]
        [Column("ClientName")]
        public string ClientName { get; set; }

        /// <summary>
        /// Комментарий клиента к заказу
        /// </summary>
        [MaxLength(CLIENT_COMMENT_MAX_LENGTH)]
        [Column("ClientComment")]
        public string ClientComment { get; set; }

        /// <summary>
        /// Комментарий мастера к заказу
        /// </summary>
        [Column("MasterComment")]
        [MaxLength(MASTER_COMMENT_MAX_LENGTH)]
        public string MasterComment { get; set; }

        /// <summary>
        /// Дата и время создания заказа (в UTC)
        /// </summary>
        [Column("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Время, на которое назначен прием для клиента (ко скольки клиент должен подойти) (в UTC)
        /// </summary>
        [Column("AppointedTime")]
        public DateTime? AppointedTime { get; set; }

        /// <summary>
        /// Состояние заказа
        /// </summary>
        [Column("OrderState")]
        public OrderState OrderState { get; set; }



        #region Навигационные свойства

        /// <summary>
        /// Навигационное свойство. Фирма
        /// </summary>
        [JsonIgnore]
        public virtual Company Company { get; set; }

        #endregion
    }
}
