﻿namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Аргументы для команды пометить заказа как выполненный
    /// </summary>
    public class FinishOrderCommandArgs : BaseCommandArguments
    {
        public long OrderId { get; set; }
    }
}
