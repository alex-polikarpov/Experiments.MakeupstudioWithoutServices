﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Пометить заказ как выполненный
    /// </summary>
    internal class FinishOrderCommand : BaseCommand<FinishOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private Order _order;

        public FinishOrderCommand(ILoggerUtility loggerUtility, IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(FinishOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            _order.OrderState = OrderState.Finished;
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(FinishOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            //Проверяем что пользователь и фирма не заблокированы
            //
            var isActive = await _unitOfWork.AccountRepository.UserAndCompanyIsActive(
                arguments.CurrentUser.CompanyId, arguments.CurrentUser.Id);

            if(!isActive)
                return result.AddError(Errors.ErrorConstants.System.ACCESS_DENIED, 100);

            _order = await _unitOfWork.OrderRepository.GetOrder(arguments.CurrentUser.CompanyId, arguments.OrderId);

            if (_order == null || _order.OrderState != OrderState.Accepted)
                return result.AddError(Errors.ErrorConstants.Orders.ORDER_NOT_FOUND, 100);

            return result;
        }
    }
}
