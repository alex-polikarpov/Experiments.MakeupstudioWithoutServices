﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Dto;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда удаления заказа
    /// </summary>
    internal class DeleteOrderCommand : BaseCommand<DeleteOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private Order _order;

        public DeleteOrderCommand(ILoggerUtility loggerUtility, IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
        }

        protected async override Task<EmptyCommandResult> InternalExecute(DeleteOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};            
            _unitOfWork.OrderRepository.Remove(_order);
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected async override Task<EmptyCommandResult> Validate(DeleteOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            _order = await _unitOfWork.OrderRepository.GetOrder(arguments.CurrentUser.CompanyId, arguments.OrderId);

            if (_order == null)
                return result.AddError(Errors.ErrorConstants.Orders.ORDER_NOT_FOUND, 100);

            return result;
        }
    }
}
