﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда для того, чтобы назначить встречу с клиентом (мастер назначает)
    /// </summary>
    internal class AppointMeetingCommand : BaseCommand<AppointMeetingCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private Order _order;

        public AppointMeetingCommand(ILoggerUtility loggerUtility, IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(AppointMeetingCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            _order.OrderState = OrderState.Accepted;
            _order.MasterComment = arguments.MasterComment;
            _order.AppointedTime = arguments.AppointedTime;
            await _unitOfWork.SaveChangesAsync();

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(AppointMeetingCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            var isActive = await _unitOfWork.AccountRepository
                .UserAndCompanyIsActive(arguments.CurrentUser.CompanyId, arguments.CurrentUser.Id);

            if (!isActive)
                return result.AddError(Errors.ErrorConstants.System.ACCESS_DENIED, 100);

            _order = await _unitOfWork.OrderRepository.GetOrder(arguments.CurrentUser.CompanyId, arguments.OrderId);

            if (_order == null || _order.OrderState != OrderState.NotAccepted)
                return result.AddError(Errors.ErrorConstants.Orders.BID_NOT_FOUND, 100);

            var canAppointMeeting = await _unitOfWork.OrderRepository.CanChangeAppointedTime(
                arguments.CurrentUser.CompanyId, arguments.OrderId, arguments.AppointedTime);

            if (!canAppointMeeting)
                return result.AddError(Errors.ErrorConstants.Orders.HAS_ANOTHER_ORDERS, 1000);

            return result;
        }
    }
}
