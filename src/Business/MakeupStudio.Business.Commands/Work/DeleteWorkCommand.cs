﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands.Work
{
    /// <summary>
    /// Команда удаления услуги
    /// </summary>
    internal class DeleteWorkCommand : BaseCommand<DeleteWorkCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private Core.Entities.Work _work;

        public DeleteWorkCommand(ILoggerUtility loggerUtility, IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
        }

        protected async override Task<EmptyCommandResult> InternalExecute(DeleteWorkCommandArgs arguments)
        {
            _unitOfWork.WorkRepository.Remove(_work);
            await _unitOfWork.SaveChangesAsync();
            return new EmptyCommandResult() { IsSuccess = true };
        }

        protected async override Task<EmptyCommandResult> Validate(DeleteWorkCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            _work = await _unitOfWork.WorkRepository.GetWork(arguments.WorkId, arguments.CurrentUser.CompanyId);

            if (_work == null)
                return result.AddError(Errors.ErrorConstants.Works.NOT_FOUND, 100);

            return result;
        }
    }
}