﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands.Work
{
    internal class AddWorkCommand : BaseCommand<AddWorkCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;

        public AddWorkCommand(ILoggerUtility loggerUtility, IUnitOfWork unitOfWork) : base(loggerUtility)
        {
             _unitOfWork = unitOfWork;
        }

        protected async override Task<EmptyCommandResult> InternalExecute(AddWorkCommandArgs arguments)
        {
            _unitOfWork.WorkRepository.Add(new Core.Entities.Work
            {
                CategoryId = arguments.CategoryId,
                Name = arguments.WorkName,
                CompanyId = arguments.CurrentUser.CompanyId,
                Price = arguments.Price
            });

            await _unitOfWork.SaveChangesAsync();
            return new EmptyCommandResult() {IsSuccess = true};
        }

        protected async override Task<EmptyCommandResult> Validate(AddWorkCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            arguments.WorkName = arguments.WorkName?.Trim();

            var workIsExists = await _unitOfWork.WorkRepository.HasWorkName(arguments.CurrentUser.CompanyId, 
                arguments.CategoryId, arguments.WorkName);

            if (workIsExists)
                return result.AddError(Errors.ErrorConstants.Works.ALREADY_EXISTS, 1000);

            var category = await _unitOfWork.WorkRepository.GetCategory(arguments.CategoryId);

            if (category == null || category.ParentCategoryId == null)
                return result.AddError(Errors.ErrorConstants.Works.CATEGORY_NOT_FOUND, 1000);

            return result;
        }
    }
}
