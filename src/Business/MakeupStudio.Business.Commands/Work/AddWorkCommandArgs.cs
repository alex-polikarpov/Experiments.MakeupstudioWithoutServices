﻿namespace MakeupStudio.Business.Commands.Work
{
    /// <summary>
    /// Аргументы для команды добавления услуги
    /// </summary>
    public class AddWorkCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Айди категории
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Название новой услуги
        /// </summary>
        public string WorkName { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Price { get; set; }
    }
}