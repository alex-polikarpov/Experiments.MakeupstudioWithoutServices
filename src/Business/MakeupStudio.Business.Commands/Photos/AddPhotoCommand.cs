﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands.Photos
{
    /// <summary>
    /// Команда добавления новой фотографии
    /// </summary>
    internal class AddPhotoCommand : BaseCommand<AddPhotoCommandArgs, EmptyCommandResult>
    {
        private readonly IFileSystemUtility _fileSystemUtility;
        private readonly IPhotosUtility _photosUtility;
        private const int _MAX_PHOTOS_COUNT = 10;
        private readonly IUnitOfWork _unitOfWork;

        public AddPhotoCommand(
            ILoggerUtility loggerUtility,
            IFileSystemUtility fileSystemUtility,
            IPhotosUtility photosUtility,
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _fileSystemUtility = fileSystemUtility;
            _photosUtility = photosUtility;
            _unitOfWork = unitOfWork;
        }



        protected override async Task<EmptyCommandResult> InternalExecute(AddPhotoCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            string newAbsolutePath = null;
            string newRelativePath = null;

            try
            {
                newAbsolutePath = _photosUtility.MoveToPhotosDirectory(arguments.TempPhotoPath);
                newRelativePath = _fileSystemUtility.GetRelativeFilePath(newAbsolutePath);
            }
            catch (Exception ex)
            {
                _fileSystemUtility.DeleteFile(arguments.TempPhotoPath);
                LoggerUtility.Error("Не удалось переместить фотку в папку с фотками");
                throw;
            }

            try
            {                
                _unitOfWork.PhotoRepository.Add(new Photo()
                {
                    CompanyId = arguments.CurrentUser.CompanyId,
                    PhotoType = PhotoType.Album,
                    RelativePath = newRelativePath
                });

                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _fileSystemUtility.DeleteFile(newAbsolutePath);
                throw;
            }

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(AddPhotoCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            var photosCount = await _unitOfWork.PhotoRepository.GetPhotosCount(arguments.CurrentUser.CompanyId);

            if (photosCount >= _MAX_PHOTOS_COUNT)
            {
                return result.AddError(Errors.ErrorConstants.Photos.MAX_COUNT, 1000);
            }

            return result;
        }
    }
}
