﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Dto;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands.Photos
{
    /// <summary>
    /// Команда удаления фотографии
    /// </summary>
    internal class DeletePhotoCommand : BaseCommand<DeletePhotoCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPhotosUtility _photosUtility;
        private readonly IFileSystemUtility _fileSystemUtility;
        private Photo _photo;

        public DeletePhotoCommand(
            ILoggerUtility loggerUtility,
            IUnitOfWork unitOfWork,
            IPhotosUtility photosUtility,
            IFileSystemUtility fileSystemUtility) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
            _photosUtility = photosUtility;
            _fileSystemUtility = fileSystemUtility;
        }



        protected override async Task<EmptyCommandResult> InternalExecute(DeletePhotoCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            var absolutePath = _fileSystemUtility.GetAbsoluteFilePath(_photo.RelativePath);
            _unitOfWork.PhotoRepository.Remove(_photo);
            await _unitOfWork.SaveChangesAsync();
            RemovePhotoFile(absolutePath);
            return result;
        }

        protected async override Task<EmptyCommandResult> Validate(DeletePhotoCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            _photo = await _unitOfWork.PhotoRepository.GetPhoto(arguments.CurrentUser.CompanyId, arguments.PhotoId);

            if (_photo == null)
                return result.AddError(Errors.ErrorConstants.Photos.NOT_FOUND, 100);

            return result;
        }


        private void RemovePhotoFile(string absolutePath)
        {
            try
            {
                _fileSystemUtility.DeleteFile(absolutePath);
            }
            catch (Exception ex)
            {
                LoggerUtility.Warn($"Не удалось удалить файл фотографии {absolutePath}", ex);
            }
        }
    }
}
