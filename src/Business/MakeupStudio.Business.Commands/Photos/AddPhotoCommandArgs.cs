﻿namespace MakeupStudio.Business.Commands.Photos
{
    /// <summary>
    /// Аргументы для команды добавления новой фотографии
    /// </summary>
    public class AddPhotoCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Путь к новой фотографии, сохраненной во временной директории
        /// </summary>
        public string TempPhotoPath { get; set; }
    }
}
