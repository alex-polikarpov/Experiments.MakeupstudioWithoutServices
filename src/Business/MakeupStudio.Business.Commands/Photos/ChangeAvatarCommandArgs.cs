﻿namespace MakeupStudio.Business.Commands.Photos
{
    /// <summary>
    /// Аргументы для команды изменения аватарки
    /// </summary>
    public class ChangeAvatarCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Путь к новой аваратке, сохраненной во временной директории
        /// </summary>
        public string TempAvatarPath { get; set; }
    }
}
